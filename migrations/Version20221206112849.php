<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221206112849 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE disponibilite CHANGE date date VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE patient ADD date_naissance VARCHAR(255) NOT NULL, DROP data_naissance');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE disponibilite CHANGE date date DATE NOT NULL');
        $this->addSql('ALTER TABLE patient ADD data_naissance DATE NOT NULL, DROP date_naissance');
    }
}
