<?php
namespace App\EventSubscriber;
namespace App\Controller;
use App\Repository\DisponibiliteRepository;
use App\Repository\DocteurRepository;
use App\Repository\PatientRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use function PHPUnit\Framework\isNull;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     * @param Request $request
     */
    public function index(DisponibiliteRepository $disponibiliteRepository, Request $request): Response
    {
        if($request->isXmlHttpRequest())
        {
            $date = $request->get('date');
            $docteursbydate = $disponibiliteRepository->findBy(['date'=> $date]);

            return $this->render('home/index.html.twig', [
                'controller_name' => 'HomeController',
                'docteurs' => $docteursbydate
            ]);
        }

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'docteurs' => []
        ]);

    }
    /**
     *
     * @Route("/ajax", name="ajax")
     * @param Request $request
     */
/*
    public function ajaxAction(DisponibiliteRepository $disponibiliteRepository, Request $request): Response
    {
        $date = $request->get('date');

        $docteursbydate = $disponibiliteRepository->findBy(['date'=> $date]);
        $docteursbydate->serialize()
        return new JsonResponse(array('listeArchive' => $docteursbydate));
    }
*/
}
