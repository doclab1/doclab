<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Patient;
use App\Form\InscriptionPatientType;

class PatientInscriptionController extends AbstractController
{
    /**
     * @Route("/patient/inscription", name="app_patient_inscription")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $patient = new Patient();
        $form = $this->createForm(InscriptionPatientType::class, $patient);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($patient);
            $em->flush();
        }

        return $this->render('patient_inscription/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
