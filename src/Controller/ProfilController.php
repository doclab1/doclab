<?php

namespace App\Controller;

use App\Repository\DisponibiliteRepository;
use App\Repository\DocteurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil/{docteur}", name="app_profil")
     */
    public function index(DocteurRepository $docteurRepo , DisponibiliteRepository $disponibiliteRepository, $docteur): Response
    {

        $docteurs = $docteurRepo->findBy(['id'=> $docteur]);
        $dispos = $disponibiliteRepository->findBy(['idDocteur' => $docteur]);


        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
            'docteurs' => $docteurs,
            'dispos' => $dispos,
            'id' => $docteur,


        ]);


    }
}
