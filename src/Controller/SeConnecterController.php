<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SeConnecterController extends AbstractController
{
    /**
     * @Route("/seconnecter", name="app_seconnecter")
     */
    public function index(): Response
    {
        return $this->render('se_connecter/index.html.twig', [
            'controller_name' => 'SeConnecterController',
        ]);
    }
}
