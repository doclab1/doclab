<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Docteur;
use App\Form\InscriptionDocteurType;

class DocteurInscriptionController extends AbstractController
{
    /**
     * @Route("/docteur/inscription", name="app_docteur_inscription")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $docteur = new docteur();
        $form = $this->createForm(InscriptionDocteurType::class, $docteur);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($docteur);
            $em->flush();
        }

        return $this->render('Docteur_inscription/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
