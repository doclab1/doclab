<?php

namespace App\Entity;

use App\Repository\AffecterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AffecterRepository::class)
 */
class Affecter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Rdv::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $idRdv;

    /**
     * @ORM\ManyToOne(targetEntity=Docteur::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $idDocteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdRdv(): ?Rdv
    {
        return $this->idRdv;
    }

    public function setIdRdv(?Rdv $idRdv): self
    {
        $this->idRdv = $idRdv;

        return $this;
    }

    public function getIdDocteur(): ?Docteur
    {
        return $this->idDocteur;
    }

    public function setIdDocteur(?Docteur $idDocteur): self
    {
        $this->idDocteur = $idDocteur;

        return $this;
    }
}
